package com.example.parkere;

import com.example.parkere.models.Flashcard;
import com.example.parkere.models.FlashcardList;
import com.example.parkere.models.Language;
import com.example.parkere.models.ReviewSession;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SessionTest {

    @Test
    void SessionExampleTest() throws IOException {
        FlashcardList flashcardList = new FlashcardList("Esperanto - difficult words", new File("src/data/imports/esperanto.txt"));
        flashcardList.setDescription("Advanced esperanto vocabulary");
        flashcardList.setLearningLanguage(new Language("Esperanto", "eo"));
        flashcardList.setTeachingLanguage(new Language("Polish", "pl"));

        System.out.println(flashcardList);

        ReviewSession rev = new ReviewSession(5, flashcardList);
        for (int i = 0; i < rev.getLength(); ++i) {
            Flashcard f = rev.nextFlashcard();
            System.out.println(f.getWord() + " || " + rev.currentContext().returnContext());
            rev.validateFlip(true);
        }
        rev.end();

        System.out.println(flashcardList);
    }

    @Test
    void SerializationTest() throws IOException, ClassNotFoundException {
        FlashcardList flashcardList = new FlashcardList("Esperanto - difficult words", new File("src/data/imports/esperanto.txt"));
        flashcardList.setDescription("Advanced esperanto vocabulary");
        flashcardList.setLearningLanguage(new Language("Esperanto", "eo"));
        flashcardList.setTeachingLanguage(new Language("Polish", "pl"));

        ReviewSession rev = new ReviewSession(5, flashcardList);
        for (int i = 0; i < rev.getLength(); ++i) {
            Flashcard f = rev.nextFlashcard();
            rev.validateFlip(true);
        }
        rev.end();
        System.out.println(flashcardList);
        flashcardList = new FlashcardList(new FileInputStream("src/data/sets/Esperanto - difficult words"));
        System.out.println(flashcardList);
    }
}
