package com.example.parkere;

import com.example.parkere.models.FlashcardList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static com.example.parkere.AlertBoxController.displayAlert;

public class ImportSetController {

    private FlashcardList importedSet;

    @FXML
    Button chooseFileButton;
    @FXML
    Button saveButton;

    @FXML
    public void initialize() {
        FileChooser fileChooser = new FileChooser();
        chooseFileButton.setOnAction(
                event -> {
                    File file = fileChooser.showOpenDialog(null);
                    try {
                        handleChosenFile(file);
                    } catch (IOException | ClassNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                }
        );
    }

    @FXML
    void goToMainScene(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("MenuScene.fxml"));
        Parent root = loader.load();
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void saveImportedSet() throws IOException {
        if (importedSet == null) {
            displayAlert("You didn't choose a file.");
            return;
        }
        importedSet.save();
        importedSet = null;
        chooseFileButton.setText("Choose file");
    }

    private void handleChosenFile(File file) throws IOException, ClassNotFoundException {
        if (file == null) {
            importedSet = null;
            chooseFileButton.setText("Choose file");
        } else {
            importedSet = new FlashcardList(new FileInputStream(file.getAbsolutePath()));
            chooseFileButton.setText(file.getName());
        }
    }
}
