package com.example.parkere.models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.locks.ReadWriteLock;

import static java.lang.Math.min;

enum QuestionType {
    FLIP, MULT_CHOICE, TYPE_ANSWER
}

public class ReviewSession {
    int length;
    int current_flashcard_no;
    ArrayList<Flashcard> cards;
    ArrayList<Context> contexts;
    FlashcardList flashcardList;

    public ReviewSession(int n, FlashcardList flashcardList) throws IOException {
        this.length = min(flashcardList.size(), n);
        cards = new ArrayList<>(n);
        contexts = new ArrayList<>(n);
        for (int i = 0; i < length; ++i) {
            Flashcard f = flashcardList.getAndRemoveFirst();
            cards.add(f);
            contexts.add(new Context(f));
        }
        current_flashcard_no = -1;
        this.flashcardList = flashcardList;
    }

    void updateScore(QuestionType questionType, ResultType resultType) {
        int add_to_score = 0;
        if (resultType == ResultType.OK) {
            if (questionType == QuestionType.TYPE_ANSWER) add_to_score = 100;
            else if (questionType == QuestionType.MULT_CHOICE) add_to_score = 50;
            else add_to_score = 40;
        } else if (resultType == ResultType.ALMOST) {
            add_to_score = 30;
        } else if (resultType == ResultType.WRONG) {
            if (questionType == QuestionType.TYPE_ANSWER) add_to_score = -30;
            else if (questionType == QuestionType.MULT_CHOICE) add_to_score = -80;
            else add_to_score = -50;
        }
        int current_score = cards.get(current_flashcard_no).getScore();
        current_score += add_to_score/min(10, cards.get(current_flashcard_no).getHistorySize() + 1);
        if (current_score < 0) current_score = 0;
        if (current_score > 100) current_score = 100;
        cards.get(current_flashcard_no).setScore(current_score);

        cards.get(current_flashcard_no).addToHistory(new HistoryRecord(resultType, questionType, System.currentTimeMillis()));
    }

    public Flashcard nextFlashcard() {
        if (current_flashcard_no == length - 1) return null;
        return cards.get(++current_flashcard_no);
    }

    public Context currentContext() {
        return contexts.get(current_flashcard_no);
    }

    public ResultType validateFlip(boolean known) {
        ResultType resultType = known ? ResultType.OK : ResultType.WRONG;
        updateScore(QuestionType.FLIP, resultType);
        return resultType;
    }

    public ResultType validateMultChoice(boolean correct) {
        ResultType resultType = correct ? ResultType.OK : ResultType.WRONG;
        updateScore(QuestionType.MULT_CHOICE, resultType);
        return resultType;
    }

    public ResultType validateType(String answer) {
        String correct = cards.get(current_flashcard_no).getWord().toLowerCase();
        ResultType resultType;
        if (answer.toLowerCase().equals(correct)) resultType = ResultType.OK;
        else {
            // condition for ALMOST
            resultType = ResultType.WRONG;
        }
        updateScore(QuestionType.TYPE_ANSWER, resultType);
        return resultType;
    }

    public int getLength() {
        return length;
    }

    public void end() throws IOException {
        flashcardList.addAll(cards);
        flashcardList.save();
    }

    public void restart(int n) throws IOException {
        end();
        this.length = min(flashcardList.size(), n);
        cards = new ArrayList<>(n);
        contexts = new ArrayList<>(n);
        for (int i = 0; i < length; ++i) {
            Flashcard f = flashcardList.getAndRemoveFirst();
            cards.add(f);
            contexts.add(new Context(f));
        }
        current_flashcard_no = -1;
    }
}
